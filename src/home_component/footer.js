import React from 'react';

function Header() {
  return (
    <div className="App">
      <p id="back-top">
        <a href="#top"><i class="fa fa-angle-up"></i></a>
      </p>
      <footer>
        <div class="container text-center">
          <p>Designed by <a>Hilyathul Wahid</a></p>
        </div>
      </footer>
    </div>
  );
}

export default Header;
