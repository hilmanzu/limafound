import React, { Component } from 'react'
import firebase from 'firebase'

class body extends Component {

    constructor(props){
        super(props);
        this.state = {
          data :'',
          loading:''
        }  
      }

    componentWillMount(){
      const db = firebase.firestore();
      const docRef = db.collection("web_limaf").doc("0.data").collection("Visi").doc("chC5aH6ZxshIbDbIsuGn")

      docRef.onSnapshot( async (doc) => {
        let data = doc.data()
        this.setState({ data:data,loading:false })
      })
    }
    render() {
        const {data}= this.state

        return (
           <section class="section">
            <div class="container" >
              <div class="row">

                <div class="col-md-5">
                  <img style={{ width:450,height:340 }} src={data.img} alt=""/>
                </div>

                <div class="col-md-7">
                  <h1 class="title" style={{marginBottom:1}}>
                    {data.nama}
                  </h1>
                  <h2 class="title" style={{marginBottom:10}}>____</h2>
                  <div class="title-line-1"></div>
                  <div class="subtitle">
                    <p>&nbsp;</p>
                    <p>{data.deskripsi}</p>
                    <p>&nbsp;</p>
                    <p><strong>{data.owner}</strong></p>
                  </div>
                </div>
                
              </div>
            </div>
          </section>
        );
    }
}

export default body