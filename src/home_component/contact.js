import React,{Component} from 'react';
import firebase from 'firebase'
import Loading from './assets/loading.js'

export default class Header extends Component{

    constructor(props) {
      super(props)
        this.state = {
          loading : true,
          data : '',
        }
    }

    componentDidMount(){
        const db = firebase.firestore();
        const docRef = db.collection("web_limaf").doc("0.data").collection("Contact").doc("data")

        docRef.onSnapshot( async (doc) => {
          let data = doc.data()
          this.setState({ data:data,loading:false })
        })

    }

    render() {
      const { data,loading} = this.state

      if (loading === true){
        return(
          <div className="App">
            <section id="about" class="light-bg">
              <div class="container">
                <div class="col-lg-12 text-center">
                  <Loading/>
                  <div class="intro-heading"> </div>
                  <div class="col-lg-12 text-center">Loading</div>
                </div>
              </div>
            </section>
          </div>
        )
      }

    return (
      <div className="App">
        <section id="contact">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 text-center">
                <div class="section-title">
                  <h2>{data.judul}</h2>
                  <p>{data.subjudul}<br/>Silakan Hubungi Kami!</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <h3>Kantor Bisnis Kami</h3>
                <p>{data.alamat}</p>
                <p><i class="fa fa-phone"></i> {data.ponsel}</p>
                <p><i class="fa fa-envelope"></i> {data.email}</p>
              </div>
              <div class="col-md-3">
                <h3>Waktu Kerja</h3>
                <p><i class="fa fa-clock-o"></i> <span class="day">Weekdays:</span><span> 9am to 5pm</span></p>
                <p><i class="fa fa-clock-o"></i> <span class="day">Saturday:</span><span> 9am to 2pm</span></p>
                <p><i class="fa fa-clock-o"></i> <span class="day">Sunday:</span><span> Closed</span></p>
              </div>
              <div class="col-md-6">
                <form name="sentMessage" id="contactForm" novalidate="">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Nama anda *" id="name" required="" data-validation-required-message="Please enter your name."/>
                        <p class="help-block text-danger"></p>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email anda *" id="email" required="" data-validation-required-message="Please enter your email address."/>
                        <p class="help-block text-danger"></p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <textarea class="form-control" placeholder="Pesan Anda *" id="message" required="" data-validation-required-message="Please enter a message."></textarea>
                        <p class="help-block text-danger"></p>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                      <div id="success"></div>
                      <button type="submit" class="btn">Kirim Pesan</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}