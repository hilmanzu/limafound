import React,{Component} from 'react';
import firebase from 'firebase'
import Loading from './assets/loading.js'

export default class Team extends Component{

	constructor(props) {
	    super(props)
	      this.state = {
	        loading : true,
	        data : '',
	        items:[]
	    }
	}

  	componentDidMount(){
      const db = firebase.firestore();
      const docRef = db.collection("web_limaf").doc("0.data").collection("Team_header").doc("data")
      const item = db.collection("web_limaf").doc("0.data").collection("Team_list").orderBy('urutan', 'asc')

      docRef.onSnapshot( async (doc) => {
        let data = doc.data()
        this.setState({ data:data })
      })

      item.onSnapshot( async (doc) => {
        var data = []
          doc.forEach((docs)=>{
            let item = data
            item.push({
              data : docs.data(),
              id   : docs.id
            })
          })
        this.setState({
          items : data,loading:false
        })
      })

 	 }


	render () {
	const {data,loading,items} = this.state

	if (loading === true){
      return(
        <div className="App">
          <section id="about" class="light-bg">
            <div class="container">
              <div class="col-lg-12 text-center">
                <Loading/>
                <div class="intro-heading"> </div>
                <div class="col-lg-12 text-center">Loading</div>
              </div>
            </div>
          </section>
        </div>
      )
    }

  	return (
	    <div className="App">
	      <section id="team" class="light-bg">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<div class="section-title">
								<h2>{data.judul}</h2>
								<p>{data.subjudul}</p>
							</div>
						</div>
					</div>
					<div class="row">
						{items.map((data)=>
							<div class="col-md-3">
								<div class="team-item">
									<div class="team-image">
										<img src={data.data.img} class="img-responsive" alt="author"/>
									</div>
									<div class="team-text">
										<h3>{data.data.nama}</h3>
										<div class="team-location">{data.data.asal}</div>
										<div class="team-position">{data.data.jabatan}</div>
										<p>{data.data.keterangan}</p>
									</div>
								</div>
							</div>
						)}
					</div>
				</div>
			</section>
	    </div>
	  );
	}
}
