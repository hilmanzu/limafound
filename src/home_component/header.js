import React,{Component} from 'react';
import firebase from 'firebase'
import Loading from './assets/loading.js'

export default class Header extends Component{

  constructor(props) {
    super(props)
      this.state = {
        loading : true,
        data : ''
      }
  }

  componentDidMount(){
      const db = firebase.firestore();
      const docRef = db.collection("web_limaf").doc("0.data").collection("Header").doc("data")

      docRef.onSnapshot( async (doc) => {
        let data = doc.data()
        this.setState({ data:data,loading:false })
      })

  }

  render(){
    const { data,loading } = this.state

    if (loading === true){
      return(
        <div className="App">
          <header>
            <div class="container">
              <div class="slider-container">
                <div class="intro-text">
                  <Loading/>
                  <div class="intro-heading"> </div>
                  <div class="intro-lead-in">Loading</div>
               </div>
              </div>
            </div>
          </header>
        </div>
      )
    }

    return (
      <div className="App">
        <header>
          <div class="container">
            <div class="slider-container">
              <div class="intro-text">
                <div class="intro-heading">{data.judul}</div>
                <div class="intro-lead-in">{data.subjudul}</div>
              </div>
            </div>
          </div>
        </header>
      </div>
    );
  }

}
