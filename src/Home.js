import React from 'react';
import Header from './home_component/header'
import Navbar from './home_component/navbar'
import About from './home_component/about'
import Visi from './home_component/visi'
import Pengguna from './home_component/pengguna'
import Portofolio from './home_component/portofolio'
import Partner from './home_component/partner'
import Team from './home_component/team'
import Contact from './home_component/contact'
import Footer from './home_component/footer'


function App() {
  return (
    <div id="page-top" className="App">
      <Navbar/>
      <Header/>
      <About/>
      <Portofolio/>
      <Team/>
      <Contact/>
      <Footer/>
    </div>
  );
}

export default App;
