import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import home from './Home.js'
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const App = () => (
  <BrowserRouter>
      <Switch>
        <Route path="/" component={home} />
      </Switch>
  </BrowserRouter>
)

export default App;

var config = {
    apiKey: "AIzaSyBD5TvUvDFEV4fnvI-EyfeC7HJZ_9TFJ7A",
    authDomain: "limafoundation.firebaseapp.com",
    databaseURL: "https://limafoundation.firebaseio.com",
    projectId: "limafoundation",
    storageBucket: "limafoundation.appspot.com",
    messagingSenderId: "964837701157",
    appId: "1:964837701157:web:0006cfdf1c7cc094"
  };
firebase.initializeApp(config);
  
